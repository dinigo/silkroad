package com.silkroad.pedidos;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Test unitario para la clase Cesta
 */
public class CestaTest extends TestCase {

    /**
     * Constructor del test
     *
     * @param testName nombre del test
     */
    public CestaTest( String testName) {
        super( testName );
    }

    /**
     * Devuelve la propia clase a testear.
     *
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite( CestaTest.class );
    }

    /**
     * Test de la funcionalidad de la clase
     */
    public void testFuncion() {
        assertTrue( true );
    }
}
