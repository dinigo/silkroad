package com.silkroad.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Constantes compartidas por varias clases
 */
public class Constants {
    // Beans asociados al a sesión //
    public static final String CLIENT_BEAN = "clientBean";
    public static final String PRODUCT_BEAN = "currentProduct";
    public static final String SELLER_BEAN = "currentSeller";
    public static final String CART_BEAN = "currentCart";
    public static final String EMPLOYEE_BEAN = "currentEmployee";

    public static final String LOGIN_SUCCESS = "cliente.jsp";
    public static final String LOGIN_FAIL = errorEncoder("Fallo de autenticación", "No se pudo iniciar seseión con este usuario");

    // Páginas a las que redirigir como resultado de operaciones de los CLIENTES //
    public static final String CLIENT_REGISTER_SUCCESS = "index.html";
    public static final String CLIENT_REGISTER_FAIL = errorEncoder("Error de registro", "No se pudo completar el registro");
    public static final String CLIENT_PASSWORD_CHANGE_SUCCESS = "cliente.jsp";
    public static final String CLIENT_PASSWORD_CHANGE_FAIL = errorEncoder("Contraseña incorrecta", "La contraseña introducida es incorrecta o la nueva contraseña no coincide con la confirmación");
    public static final String CLIENT_PROFILE_CHANGED = "cliente.jsp";
    public static final String CLIENT_PROFILE_REMOVED = "intex.html";

    // Páginas a las que redirigir como resultado de operaciones de los VENDEDORES //
    public static final String SELLER_REGISTER_SUCCESS = "control_pane.jsp";
    public static final String SELLER_REGISTER_FAIL = errorEncoder("Error de registro", "No se pudo completar el registro");
    public static final String SELLER_UPDATE_FAIL = errorEncoder("Error al actualizar un vendedor", "No se pudieron actualizar los datos del perfil del vendedor");
    public static final String SELLER_PROFILE_CHANGED = "vendedor.jsp";
    public static final String SELLER_PROFILE = "vendedor.jsp";
    public static final String SELLER_REMOVED = "control_pane.jsp";

    // Otras páginas //
    public static final String FORM_FAIL = errorEncoder("Formulario incorrecto", "Intenta enviar información desde un formulario no soportado");

    // Páginas a las que redirigir como resultado de operaciones de los PRODUCTOS //
    public static final String PRODUCT_CHANGE_FAIL = errorEncoder("Error al modificar un producto", "No se pudo modificar el producto");
    public static final String PRODUCT_CHANGE_SUCCESS = "producto.jsp?id=" ;
    public static final String PRODUCT_REMOVE = "control_pane.jsp" ;

    // Páginas a las que redirigir como resultado de operaciones de los EMPLEADOS //
    public static final String EMPLOYEE_REGISTER_FAIL = errorEncoder("Error al registrar un empleado", "No se pudoregistrar el nuevo empleado");
    public static final String EMPLOYEE_REGISTER_SUCCESS = "index.html";
    public static final String EMPLOYEE_UPDATE_FAIL = errorEncoder("Error al actualizar un empleado", "No se pudieron actualizar los datos del perfil del empleado");
    public static final String EMPLOYEE_UPDATE_SUCCESS = "empleado.jsp";
    public static final String EMPLOYEE_PROFILE_REMOVED = "control_pane.jsp";
    public static final String EMPLOYEE_LOGIN_SUCCESS = "control_pane.jsp";

    // Páginas a las que redirigir como resultado de operaciones de los PRODUCTOS //
    public static final String PRODUCT_REGISTER_FAIL = errorEncoder("Error al crear un producto", "Ocurrió un error al procesar el formulario de creación de producto");
    public static final String PRODUCT_REGISTER_SUCCESS = "control_pane.jsp";
    public static final String PRODUCT_UPDATE_FAIL = errorEncoder("Error al actualizar un producto", "Ocurrió un error al procesar el formulario de actualización de producto");
    public static final String PRODUCT_UPDATE_SUCCESS = "control_pane.jsp";

    // Páginas a las que redirigir como resultado de operaciones del CATÁLOGO //
    public static final String CATALOG_UPDATE_FAIL = errorEncoder("Error al actualizar un el catálogo", "Ocurrió un error al procesar el formulario de actualización de producto del catálogo de un vendedor");
    public static final String CATALOG_REGISTER_FAIL = errorEncoder("Error al registrar un el catálogo", "Ocurrió un error al procesar el formulario de registro de producto del catálogo de un vendedor");

    /**
     * Función que ayuda a generar la dirección a una página de error fácilmente.
     *
     * @param code        código del error o raźon
     * @param description explicación de por qué ocurrió el error, más extensa que el código
     * @return url de la página de error
     */
    public static String errorEncoder(String code, String description) {
        String errorUrl = "error.jsp";
        try {
            errorUrl += "?code=" + code + "&" + URLEncoder.encode(description, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return errorUrl;
    }
}