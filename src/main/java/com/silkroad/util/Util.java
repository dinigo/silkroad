package com.silkroad.util;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.db.HsqldbDatabaseType;
import com.j256.ormlite.jdbc.DataSourceConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public class Util {

    /**
     * Esquema básico de los recursos concatenado al nombre del JDNI de la base de datos.
     */
    private static final String DATASOURCE = "java:/comp/env/jdbc/tienda";

    /**
     * Carpeta donde se encuentran los componentes que incrustar
     */
    private static final String COMPONENTS_DIR = "components";

    /**
     * Nombre del parámetro que pasar junto al get en la url
     */
    private static final String COMPONENTS_PARAM = "component";

    /**
     * Plantilla con un componente que rellenar
     */
    private static final String TEMPLATE_PAGE = "template.jsp";

    /**
     * Logger para la clase Util
     */
    static Logger log = LoggerFactory.getLogger(Util.class);

    /**
     * Recibe una petición y una respuesta http (típicamente del método doGet(...) de un servlet) y el nombre de un
     * componente para redirecciona a el si existe.
     *
     * @param request   peticion http
     * @param response  respuesta http
     * @param component componente al que se redirecciona
     */
    public static void forwardComponent(HttpServletRequest request, HttpServletResponse response, String component) {
        String forwardUrl = TEMPLATE_PAGE + "?" + COMPONENTS_PARAM + "=" + COMPONENTS_DIR + "/" + component;
        log.debug("Redirigiendo a: " + forwardUrl);
        RequestDispatcher dispatcher = request.getRequestDispatcher(forwardUrl);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException | IOException e) {
            log.error("No se pudo redireccionar al componente: " + component);
        }
    }

    /**
     * Crea un ConnectionSource de acuerdo con la dirección jnri del recurso de la base de datos declarado en el web.xml
     *
     * @return connectionSource objeto para conectarse a la base de datos
     */
    private static ConnectionSource getConnectionSource() throws SQLException {
        ConnectionSource connectionSource = null;
        try {
            Context ctx = new InitialContext();
            DataSource dataSource = (DataSource) ctx.lookup(DATASOURCE);
            connectionSource = new DataSourceConnectionSource(dataSource, new HsqldbDatabaseType() {
                /**
                 * como no hace falta el driver, porque la conexión se delega en el recurso conectado a tomcat, es
                 * necesario sobreescribir la comprobación de versión del driver (antes de la versión 1.8 no estaba
                 * soportado el VARCHAR) que hace que no se mande la longitud de los campos de la tabla.
                 */
                @Override
                public boolean isVarcharFieldWidthSupported() {
                    return true;
                }
            });
        } catch (NamingException e) {
            log.error("No se pudo crear un ConnectionSource porque la dirección jnri del recurso está mal: " + DATASOURCE, e);
        }
        return connectionSource;
    }

    /**
     * Crea un Dao conectado al jnri donde está mapeado el recurso de la base de datos.
     *
     * @param clazz es la clase que se quiere recuperar
     * @return entityDao es el dao ya instanciado para acceder a ese recurso
     * @throws SQLException error en la petición a la base de datos
     */
    public static <T> Dao<T, Integer> getDao(Class<T> clazz) throws SQLException {
        return DaoManager.createDao(getConnectionSource(), clazz);
    }

    /**
     * Aplica el algoritmo resumen SHA1 sobre una String.
     *
     * @param plain - Texto en claro
     * @return resumed - Texto pasado por el digest
     */
    public static String sha1(String plain) {
        String resumed = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA1");
            digest.reset();
            digest.update(plain.getBytes("UTF-8"));
            resumed = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return resumed;
    }
}