package com.silkroad.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.silkroad.util.Util;

import java.io.Serializable;

/**
 * Bean que contiene la información de un cliente (principalmente código boilerplate, pero qué se le va a hacer,
 * sería todavía peor si escribiese las sentencias SQL-ish (porque hsqldb ni siquiera es SQL compliant) a mano).
 * Emplea un {@link com.j256.ormlite.dao.Dao} para las operaciones CRUD en la base de datos.
 */
@DatabaseTable
public class Cliente implements Serializable {

    @DatabaseField(generatedId = true)
    private int codCliente;

    @DatabaseField(width = 100)
    private String nombre;

    @DatabaseField(width = 100)
    private String apellidos;

    @DatabaseField(width = 100)
    private String direccion;

    @DatabaseField(unique = true, width = 100)
    private String dni;

    @DatabaseField(width = 100)
    private String email;

    @DatabaseField(width = 100)
    private String contrasena;

    /**
     * Constructor por defecto, Cliente con todos los campos sin instanciar.
     */
    public Cliente() {
    }

    /**
     * Constructor con todos los parámetros necesarios.
     *
     * @param nombre     nombre del cliente
     * @param apellidos  apellidos del cliente
     * @param direccion  dirección del clienet
     * @param dni        número del dni y la letra
     * @param email      correo electrónico del cliente
     * @param contrasena contraseña para acceder (en claro)
     */
    public Cliente(String nombre, String apellidos, String direccion, String dni, String email, String contrasena) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.dni = dni;
        this.email = email;
        this.contrasena = Util.sha1(contrasena);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    /**
     * Almacena {@link com.silkroad.util.Util#sha1(String contraseña)}
     *
     * @param contrasena contraseña en claro
     */
    public void setContrasena(String contrasena) {
        this.contrasena = Util.sha1(contrasena);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "codCliente=" + codCliente +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", direccion='" + direccion + '\'' +
                ", dni='" + dni + '\'' +
                ", email='" + email + '\'' +
                ", contrasena='" + contrasena + '\'' +
                '}';
    }

    /**
     * Devuelve {@code true} si el objeto {@link com.silkroad.data.Cliente} no tiene ningún campo a {@code null} y
     * {@code false} en caso contrario
     *
     * @return isValid
     */
    public boolean isValid() {
        return nombre != null || apellidos != null || direccion != null || dni != null || email != null || contrasena != null;
    }
}