package com.silkroad.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import javax.persistence.*;

@DatabaseTable
public class Pedido {
    @DatabaseField(uniqueCombo = true)
    private int codPedido;

    @DatabaseField(uniqueCombo = true)
    @ManyToOne
    private Cliente codCliente;

    @Column
    private float precio;

    @Column
    private int stock;

    @Column
    private boolean activo;

    public Pedido() {
    }

    public int getCodPedido() {
        return codPedido;
    }

    public void setCodPedido(int codPedido) {
        this.codPedido = codPedido;
    }
}
