package com.silkroad.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Producto {
    @Id
    @GeneratedValue
    public int codProducto;

    @Column
    private String categoria;

    @Column
    private String marca;

    @Column
    private String modelo;

    @Column
    private String talla;

    @Column
    private String descripcion;

    @Column
    private boolean activo;

    public Producto() {
    }

    public Producto(String categoria, String marca, String modelo, String talla, String descripcion, boolean activo) {
        this.categoria = categoria;
        this.marca = marca;
        this.modelo = modelo;
        this.talla = talla;
        this.descripcion = descripcion;
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public int getCodProducto() {
        return codProducto;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "codProducto=" + codProducto +
                ", categoria='" + categoria + '\'' +
                ", marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", talla='" + talla + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", activo=" + activo +
                '}';
    }
}