package com.silkroad.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.silkroad.util.Util;

import java.io.Serializable;

/**
 * Bean que contiene la información de un cliente (principalmente código boilerplate, pero qué se le va a hacer,
 * sería todavía peor si escribiese las sentencias SQL-ish (porque hsqldb ni siquiera es SQL compliant) a mano).
 * Emplea un {@link com.j256.ormlite.dao.Dao} para las operaciones CRUD en la base de datos.
 */
@DatabaseTable
public class Empleado implements Serializable {

    @DatabaseField(generatedId = true)
    private int codEmpleado;

    @DatabaseField(width = 100)
    private String dni;

    @DatabaseField(width = 100)
    private String contrasena;

    @DatabaseField(width = 500)
    private String privilegios;

    public Empleado() {
    }

    public Empleado(String dni, String contrasena, String privilegios) {
        this.dni = dni;
        this.contrasena = Util.sha1(contrasena);
        this.privilegios = privilegios;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "codEmpleado=" + codEmpleado +
                ", dni='" + dni + '\'' +
                ", contrasena='" + contrasena + '\'' +
                ", privilegios='" + privilegios + '\'' +
                '}';
    }

    public boolean canModify(String privilegio) {
        return this.privilegios.contains(privilegio);
    }

    public String getContrasena() {
        return contrasena;
    }

    /**
     * Almacena {@link com.silkroad.util.Util#sha1(String contraseña)}
     *
     * @param contrasena contraseña en claro
     */
    public void setContrasena(String contrasena) {
        this.contrasena = Util.sha1(contrasena);
    }

    public int getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(int codEmpleado) {
        this.codEmpleado = codEmpleado;
    }
}