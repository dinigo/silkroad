package com.silkroad.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Vendedor {
    @DatabaseField(generatedId = true)
    private int codVendedor;

    @DatabaseField(unique = true)
    private String nombre;

    @DatabaseField(unique = true)
    private String cif;

    @DatabaseField
    private String telefono;

    @DatabaseField
    private String email;

    @DatabaseField
    private String direccion;

    @DatabaseField
    private boolean activo;

    public Vendedor() {
    }

    public Vendedor(String nombre, String cif, String telefono, String email, String direccion, boolean activo) {
        this.nombre = nombre;
        this.cif = cif;
        this.telefono = telefono;
        this.email = email;
        this.direccion = direccion;
        this.activo = activo;
    }

    public int getCodVendedor() {
        return codVendedor;
    }

    public void setCodVendedor(int codVendedor) {
        this.codVendedor = codVendedor;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Vendedor{" +
                "codVendedor=" + codVendedor +
                ", nombre='" + nombre + '\'' +
                ", cif='" + cif + '\'' +
                ", telefono='" + telefono + '\'' +
                ", email='" + email + '\'' +
                ", direccion='" + direccion + '\'' +
                ", activo=" + activo +
                '}';
    }
}