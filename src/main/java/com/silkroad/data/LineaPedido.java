package com.silkroad.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import javax.persistence.ManyToOne;
import java.sql.Date;

@DatabaseTable
public class LineaPedido {
    @DatabaseField(uniqueCombo = true)
    @ManyToOne
    private Pedido codPedido;

    @DatabaseField(uniqueCombo = true)
    @ManyToOne
    private Producto codProducto;

    @DatabaseField(uniqueCombo = true)
    @ManyToOne
    private Vendedor codVendedor;

    @DatabaseField
    private int cantidad;

    @DatabaseField
    private String estado;

    @DatabaseField
    private Date fechaEnvio;

    @DatabaseField
    private Date fechaEntrega;

    public LineaPedido() {
    }
}