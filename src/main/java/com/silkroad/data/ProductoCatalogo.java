package com.silkroad.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@DatabaseTable
public class ProductoCatalogo {
    @DatabaseField(generatedId = true)
    private int codCatalogo;

    @DatabaseField(uniqueCombo = true, foreign = true, columnName="codVendedor")
    @ManyToOne
    private Vendedor codVendedor;

    @DatabaseField(uniqueCombo = true, foreign = true, columnName="codProducto")
    @ManyToOne
    private Producto codProducto;

    @DatabaseField
    private float precio;

    @DatabaseField
    private int stock;

    @DatabaseField
    private boolean activo;

    public ProductoCatalogo() {
    }

    public ProductoCatalogo(Vendedor codVendedor, Producto codProducto, float precio, int stock, boolean activo) {
        this.codVendedor = codVendedor;
        this.codProducto = codProducto;
        this.precio = precio;
        this.stock = stock;
        this.activo = activo;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "ProductoCatalogo{" +
                "codVendedor=" + codVendedor.getCodVendedor() +
                ", codProducto=" + codProducto.getCodProducto() +
                ", precio=" + precio +
                ", stock=" + stock +
                ", activo=" + activo +
                '}';
    }
}