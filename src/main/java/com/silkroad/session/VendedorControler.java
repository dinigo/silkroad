package com.silkroad.session;

import com.j256.ormlite.dao.Dao;
import com.silkroad.data.Vendedor;
import com.silkroad.util.Constants;
import com.silkroad.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Procesa lo que tenga que ver con vendedores
 */
public class VendedorControler {

    /**
     * Logger
     */
    static Logger log = LoggerFactory.getLogger(VendedorControler.class);

    /**
     * Actualiza la información de un vendedor
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    public static void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String telefono = request.getParameter("telefono");
        String email = request.getParameter("email");
        String direccion = request.getParameter("direccion");
        int codVendedor = Integer.parseInt(request.getParameter("codVendedor"));
        boolean activo = Boolean.parseBoolean(request.getParameter("activo"));

        String nextPage = Constants.SELLER_UPDATE_FAIL;
        try {
            Dao<Vendedor, Integer> vendedorDao = Util.getDao(Vendedor.class);
            Vendedor vendedor = vendedorDao.queryForId(codVendedor);
            vendedor.setActivo(activo);
            vendedor.setDireccion(direccion);
            vendedor.setTelefono(telefono);
            vendedor.setEmail(email);
            vendedorDao.update(vendedor);
            nextPage = "vendedor.jsp?codVendedor=" + codVendedor;
        } catch (SQLException e) {
            log.error("Error al leer un vendedor de la base de dadtos");
        }
        log.debug(nextPage);
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

    /**
     * Crea un vendedor nuevo
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    public static void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nombre = request.getParameter("nombre");
        String cif = request.getParameter("cif");
        String telefono = request.getParameter("telefono");
        String email = request.getParameter("email");
        String direccion = request.getParameter("direccion");
        boolean activo = Boolean.parseBoolean(request.getParameter("activo"));

        Vendedor vendedor = new Vendedor(nombre, cif, telefono, email, direccion, activo);
        log.debug("Creado: " + vendedor.toString());
        String nextPage = Constants.SELLER_REGISTER_FAIL;
        try {
            Dao<Vendedor, Integer> vendedorDao = Util.getDao(Vendedor.class);
            vendedorDao.createIfNotExists(vendedor);
            nextPage = Constants.SELLER_REGISTER_SUCCESS;
            log.debug("Creado: " + vendedor.toString());
        } catch (SQLException e) {
            log.error("Error al añadir un producto nuevo a la base de datos");
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

    /**
     * Elimina un vendedor de la base de datos y de la sesión actual
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    public static void remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int codVendedor = Integer.parseInt(request.getParameter("codVendedor"));

        try {
            Dao<Vendedor, Integer> clientDao = Util.getDao(Vendedor.class);
            clientDao.deleteById(codVendedor);
            log.debug("Eliminado vendedor: " + codVendedor);
        } catch (SQLException e) {
            log.error("Error al eliminar un vendedor");
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(Constants.SELLER_REMOVED);
        dispatcher.forward(request, response);
    }
}