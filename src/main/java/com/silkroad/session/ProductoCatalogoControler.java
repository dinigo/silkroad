package com.silkroad.session;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.silkroad.data.Producto;
import com.silkroad.data.ProductoCatalogo;
import com.silkroad.data.Vendedor;
import com.silkroad.util.Constants;
import com.silkroad.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Procesa lo que tenga que ver con los productos
 */
public class ProductoCatalogoControler {

    /**
     * Logger
     */
    static Logger log = LoggerFactory.getLogger(ProductoCatalogoControler.class);

    public static void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        float precio = Float.parseFloat(request.getParameter("precio"));
        int stock = Integer.parseInt(request.getParameter("stock"));
        int codProducto = Integer.parseInt(request.getParameter("codProducto"));
        int codVendedor = Integer.parseInt(request.getParameter("codVendedor"));
        boolean activo = Boolean.parseBoolean(request.getParameter("activo"));

        String nextPage = Constants.CATALOG_UPDATE_FAIL;
        try {
            Dao<ProductoCatalogo, Integer> productoDao = Util.getDao(ProductoCatalogo.class);
            QueryBuilder<ProductoCatalogo, Integer> statementBuilder = productoDao.queryBuilder();
            List<ProductoCatalogo> listProduct = statementBuilder.where().eq("codProducto", codProducto).and().eq("codVendedor", codVendedor).query();
            ProductoCatalogo producto = listProduct.get(0);
            producto.setPrecio(precio);
            producto.setStock(stock);
            producto.setActivo(activo);
            productoDao.update(producto);

            nextPage = "vendedor.jsp?codVendedor=" + codVendedor;
        } catch (SQLException e) {
            log.error("Error al acceder a la base de datos", e);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

    public static void remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int codProducto = Integer.parseInt(request.getParameter("codProducto"));
        int codVendedor = Integer.parseInt(request.getParameter("codVendedor"));

        try {
            Dao<ProductoCatalogo, Integer> productoDao = Util.getDao(ProductoCatalogo.class);
            QueryBuilder<ProductoCatalogo, Integer> statementBuilder = productoDao.queryBuilder();
            List<ProductoCatalogo> listProduct = statementBuilder.where().like("codProducto", codProducto).and().like("codVendedor", codVendedor).query();
            ProductoCatalogo producto = listProduct.get(0);
            productoDao.delete(producto);
            log.debug("Eliminado producto: " + codProducto);
        } catch (SQLException e) {
            log.error("Error al eliminar un producto");
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(Constants.PRODUCT_REMOVE);
        dispatcher.forward(request, response);
    }

    public static void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int codProducto = Integer.parseInt(request.getParameter("codProducto"));
        int codVendedor = Integer.parseInt(request.getParameter("codVendedor"));
        float precio =(float) Integer.parseInt(request.getParameter("precio"));
        int stock = Integer.parseInt(request.getParameter("stock"));
        boolean activo = Boolean.parseBoolean(request.getParameter("activo"));
        log.debug("Registrar ProductoCatalogo: " + codProducto + " " + codVendedor + " " + precio + " " + stock + " " + activo);
        String nextPage = Constants.CATALOG_REGISTER_FAIL;
        try {
            Dao<Producto, Integer> productoDao = Util.getDao(Producto.class);
            Dao<Vendedor, Integer> vendedorDao = Util.getDao(Vendedor.class);
            Producto producto = productoDao.queryForId(codProducto);
            Vendedor vendedor = vendedorDao.queryForId(codVendedor);
            ProductoCatalogo productoCatalogo = new ProductoCatalogo(vendedor, producto, precio, stock, activo);
            Dao<ProductoCatalogo, Integer> productoCatalogoDao = Util.getDao(ProductoCatalogo.class);
            productoCatalogoDao.create(productoCatalogo);
            nextPage = "vendedor.jsp?codVendedor=" + codVendedor;
        } catch (SQLException e) {
            log.error("Error al acceder a la base de datos", e);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }
}