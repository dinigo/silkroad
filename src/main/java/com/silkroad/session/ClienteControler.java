package com.silkroad.session;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.silkroad.data.Cliente;
import com.silkroad.util.Constants;
import com.silkroad.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Procesa lo que tenga que ver con clientes
 */
public class ClienteControler {

    /**
     * Logger
     */
    static Logger log = LoggerFactory.getLogger(ClienteControler.class);

    /**
     * Elimina un cliente de la base de datos y de la sesión actual
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    public static void remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Cliente cliente = (Cliente) session.getAttribute(Constants.CLIENT_BEAN);
        session.removeAttribute(Constants.CLIENT_BEAN);
        try {
            Dao<Cliente, Integer> clientDao = Util.getDao(Cliente.class);
            clientDao.delete(cliente);
            log.debug("Eliminado " + cliente.toString());
        } catch (SQLException e) {
            log.error("Error al eliminar un cliente");
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(Constants.CLIENT_PROFILE_REMOVED);
        dispatcher.forward(request, response);
    }

    /**
     * Actualiza los datos de perfil del usuario
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    public static void updateProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nombre = request.getParameter("nombre");
        String apellidos = request.getParameter("apellidos");
        String direccion = request.getParameter("direccion");
        String dni = request.getParameter("dni");
        String email = request.getParameter("email");

        String nextPage = Constants.FORM_FAIL;
        HttpSession session = request.getSession();
        Cliente cliente = (Cliente) session.getAttribute(Constants.CLIENT_BEAN);
        log.debug("Perfil actual: " + cliente.toString());
        cliente.setNombre(nombre);
        cliente.setApellidos(apellidos);
        cliente.setDireccion(direccion);
        cliente.setDni(dni);
        cliente.setEmail(email);
        log.debug("Perfil modificado: " + cliente.toString());

        try {
            Dao<Cliente, Integer> clientDao = Util.getDao(Cliente.class);
            clientDao.update(cliente);
            nextPage = Constants.CLIENT_PROFILE_CHANGED;
        } catch (SQLException e) {
            log.error("Error al actualizar los datos del cliente");
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

    /**
     * Cambia la contraseña del usuario
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    public static void updatePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String passVieja = request.getParameter("vieja_contrasena");
        String passNueva = request.getParameter("nueva_contrasena");
        String passRepetida = request.getParameter("re_contrasena");

        HttpSession session = request.getSession();
        Cliente cliente = (Cliente) session.getAttribute(Constants.CLIENT_BEAN);

        String nextPage = Constants.CLIENT_PASSWORD_CHANGE_FAIL;
        // si se introdujo correctamente la contraseña
        if (cliente.getContrasena().equals(Util.sha1(passVieja))) {
            // si las nuevas contraseñas coinciden entre si
            if (passNueva.equals(passRepetida)) {
                cliente.setContrasena(passNueva);
                // actualizo aqui porque si los condicionales fallan me ahorro la query
                try {
                    Dao<Cliente, Integer> clientDao = Util.getDao(Cliente.class);
                    clientDao.update(cliente);
                    nextPage = Constants.CLIENT_PASSWORD_CHANGE_SUCCESS;
                } catch (SQLException e) {
                    log.error("Error al cambiar la contraseña");
                }
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

    /**
     * Registra un nuevo usuario
     *
     * @param request  Petición http
     * @param response Respuesta http
     * @throws ServletException
     * @throws IOException
     */
    public static void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nombre = request.getParameter("nombre");
        String apellidos = request.getParameter("apellidos");
        String direccion = request.getParameter("direccion");
        String dni = request.getParameter("dni");
        String email = request.getParameter("email");
        String contrasena = request.getParameter("contrasena");
        String confirmacion = request.getParameter("re_contrasena");

        Cliente cliente = new Cliente(nombre, apellidos, direccion, dni, email, contrasena);
        log.debug("Recibido cliente: " + cliente.toString());

        // Componente al que se redirigirá al final. Por defecto el del fallo.
        String component = Constants.CLIENT_REGISTER_FAIL;

        // Si los campos son correctos
        if (cliente.isValid() && contrasena.equals(confirmacion)) {
            // Comprueba que el dni sea único
            log.debug("Campos formulario de registro OK");
            try {
                Dao<Cliente, Integer> clientDao = Util.getDao(Cliente.class);
                int statusCode = clientDao.create(cliente);
                log.debug("Creado el cliente con éxito: " + (statusCode == 1));
                if (statusCode == 1) { // Si se ha insertado correctamente
                    component = Constants.CLIENT_REGISTER_SUCCESS;
                }
            } catch (SQLException e) {
                log.error("Error al acceder a la base de datos", e);
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(component);
        dispatcher.forward(request, response);
    }

    /**
     * Inicia sesión para un usuario ya existente. Como el diagrama entidad relación dice
     *
     * @param request  Petición http
     * @param response Respuesta http
     * @throws ServletException
     * @throws IOException
     */
    public static void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dni = request.getParameter("dni");
        String password = request.getParameter("password");
        log.debug("Intentando loguearse con parametros  " + dni + ":" + password);

        String component = Constants.LOGIN_FAIL;

        if (!dni.equals("") && !password.equals("")) { // Si los campos son correctos
            log.debug("Campos OK");
            try {
                Dao<Cliente, Integer> clientDao = Util.getDao(Cliente.class);
                List<Cliente> registeredClients = clientDao.queryForEq("dni", dni);
                if (!registeredClients.isEmpty()) {  // Si el usuario está registrado
                    Cliente cliente = registeredClients.get(0);
                    if (cliente.getContrasena().equals(Util.sha1(password))) { // La contraseña coincide
                        HttpSession session = request.getSession();
                        session.setAttribute(Constants.CLIENT_BEAN, cliente);
                        log.debug("Loggeado el usuario: " + cliente.toString());
                        component = Constants.LOGIN_SUCCESS;
                    }
                } else {
                    log.error("No hay nadie registrado con el DNI: " + dni);
                }
            } catch (SQLException e) {
                log.error("Error en la query a la base de datos: ", e);
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(component);
        dispatcher.forward(request, response);
    }
}
