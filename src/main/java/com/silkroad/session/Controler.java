package com.silkroad.session;

import com.j256.ormlite.dao.Dao;
import com.silkroad.data.Producto;
import com.silkroad.util.Constants;
import com.silkroad.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Recibe un formulario y procesa los datos en función de qué formulario sea. Esto se sabe apartir del campo oculto
 * {@code form_type}
 *
 * @author Daniel Iñigo
 */
@SuppressWarnings("serial")
@WebServlet("/controler")
public class Controler extends HttpServlet {

    /**
     * Logger
     */
    static Logger log = LoggerFactory.getLogger(Controler.class);

    /**
     * Obtiene el tipo de formulario y llama a la función correcta para procesarlo. Si el formulario no existe se
     * muestra una página de error
     *
     * @param request  Petición http
     * @param response Respuesta http
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        String formType = request.getParameter("form_type");
        switch (formType) {
            // CLIENTE //
            case "login_cliente":
                ClienteControler.login(request, response);
                break;
            case "register_cliente":
                ClienteControler.register(request, response);
                break;
            case "change_password":
                ClienteControler.updatePassword(request, response);
                break;
            case "update_client":
                ClienteControler.updateProfile(request, response);
                break;
            case "remove_cliente":
                ClienteControler.remove(request, response);
                break;

            // EMPLEADO //
            case "register_empleado":
                EmpleadoControler.register(request, response);
                break;
            case "login_empleado":
                EmpleadoControler.login(request, response);
                break;
            case "update_empleado":
                EmpleadoControler.update(request, response);
                break;
            case "remove_empleado":
                EmpleadoControler.remove(request, response);
                break;

            // VENDEDOR //
            case "register_vendedor":
                VendedorControler.register(request, response);
                break;
            case "update_vendedor":
                VendedorControler.update(request, response);
                break;
            case "remove_vendedor":
                VendedorControler.remove(request, response);
                break;

            // PPRODUCTO //
            case "register_producto":
                ProductoControler.register(request, response);
                break;
            case "update_producto":
                ProductoControler.update(request, response);
                break;
            case "remove_producto":
                ProductoControler.remove(request, response);
                break;

            // CATALOGO //
            case "register_productocatalogo":
                ProductoCatalogoControler.register(request,response);
                break;
            case "update_productocatalogo":
                ProductoCatalogoControler.update(request,response);
                break;
            case "remove_productocatalogo":
                ProductoCatalogoControler.remove(request,response);
                break;

            // PEDIDO //


            // FORMULARIO NO VALIDO //
            default:
                RequestDispatcher dispatcher = request.getRequestDispatcher(Constants.FORM_FAIL);
                dispatcher.forward(request, response);
        }
    }


    /**
     * Activa o desactiva un producto y modifica su descripción
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    private void changeProduct(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String descripcion = request.getParameter("descripcion");
        boolean activo = request.getParameter("activo").equals("si");
        HttpSession session = request.getSession();
        Producto producto = (Producto) session.getAttribute(Constants.PRODUCT_BEAN);

        String nextPage = Constants.PRODUCT_CHANGE_FAIL;
        // solo si ha cambiado alguno de los valores actualizamos la entrada de la db
        if (!producto.getDescripcion().equals(descripcion) || producto.isActivo() != activo) {
            producto.setDescripcion(descripcion);
            producto.setActivo(activo);
            try {
                Dao<Producto, Integer> productoDao = Util.getDao(Producto.class);
                productoDao.update(producto);
                nextPage = Constants.PRODUCT_CHANGE_SUCCESS + producto.getCodProducto();
            } catch (SQLException e) {
                log.error("Error al modificar un producto de la base de datos");
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

    /**
     * Inserta un producto nuevo en la base de datos
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    private void newProduct(HttpServletRequest request, HttpServletResponse response) {
        String categoria = request.getParameter("categoria");
        String marca = request.getParameter("marca");
        String modelo = request.getParameter("modelo");
        String talla = request.getParameter("talla");
        String descripcion = request.getParameter("descripcion");
        String activo = request.getParameter("activo");
        Producto producto = new Producto(categoria, marca, modelo, talla, descripcion, activo.equals("si"));

        try {
            Dao<Producto, Integer> productoDao = Util.getDao(Producto.class);
            productoDao.createIfNotExists(producto);
        } catch (SQLException e) {
            log.error("Error al añadir un producto nuevo a la base de datos");
        }
    }


}