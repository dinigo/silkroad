package com.silkroad.session;

import com.j256.ormlite.dao.Dao;
import com.silkroad.data.Empleado;
import com.silkroad.data.Producto;
import com.silkroad.data.Vendedor;
import com.silkroad.util.Constants;
import com.silkroad.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Procesa lo que tenga que ver con los productos
 */
public class ProductoControler {

    /**
     * Logger
     */
    static Logger log = LoggerFactory.getLogger(ProductoControler.class);

    public static void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String descripcion = request.getParameter("descripcion");
        boolean activo = Boolean.parseBoolean(request.getParameter("activo"));
        int codProducto = Integer.parseInt(request.getParameter("codProducto"));

        String nextPage = Constants.PRODUCT_UPDATE_FAIL;
        try {

            Dao<Producto, Integer> productoDao = Util.getDao(Producto.class);
            Producto producto = productoDao.queryForId(codProducto);
            producto.setDescripcion(descripcion);
            producto.setActivo(activo);
            productoDao.update(producto);
            nextPage = Constants.PRODUCT_UPDATE_SUCCESS;
        } catch (SQLException e) {
            log.error("Error al acceder a la base de datos", e);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

    public static void remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int codProducto = Integer.parseInt(request.getParameter("codProducto"));

        try {
            Dao<Producto, Integer> productoDao = Util.getDao(Producto.class);
            productoDao.deleteById(codProducto);
            log.debug("Eliminado producto: " + codProducto);
        } catch (SQLException e) {
            log.error("Error al eliminar un producto");
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(Constants.PRODUCT_REMOVE);
        dispatcher.forward(request, response);
    }

    public static void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String categoria = request.getParameter("categoria");
        String marca = request.getParameter("marca");
        String modelo = request.getParameter("modelo");
        String talla = request.getParameter("talla");
        String descripcion = request.getParameter("descripcion");
        boolean activo = Boolean.parseBoolean(request.getParameter("activo"));

        Producto producto = new Producto(categoria,marca,modelo,talla,descripcion,activo);
        log.debug("Creado: " + producto.toString());
        String nextPage = Constants.PRODUCT_REGISTER_FAIL;
        try {
            Dao<Producto, Integer> productoDao = Util.getDao(Producto.class);
            int statusCode = productoDao.create(producto);
            if (statusCode == 1) { // Si se ha insertado correctamente
                log.debug("Creado: " + producto.toString());
                nextPage = Constants.PRODUCT_REGISTER_SUCCESS;
            }
        } catch (SQLException e) {
            log.error("Error al acceder a la base de datos", e);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

}
