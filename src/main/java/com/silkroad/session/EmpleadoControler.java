package com.silkroad.session;

import com.j256.ormlite.dao.Dao;
import com.silkroad.data.Empleado;
import com.silkroad.util.Constants;
import com.silkroad.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Procesa lo que tenga que ver con empleados
 */
public class EmpleadoControler {

    /**
     * Logger
     */
    static Logger log = LoggerFactory.getLogger(EmpleadoControler.class);

    public static void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dni = request.getParameter("dni");
        String[] listPrivilegios = request.getParameterValues("privilegios");
        String contrasena = request.getParameter("contrasena");
        String confirmacion = request.getParameter("re_contrasena");

        // concatena los privilegios en una lista separada por comas
        String privilegios = "";
        if (listPrivilegios.length > 0) {
            for (int i = 0; i < listPrivilegios.length - 1; i++) {
                privilegios += listPrivilegios[i] + ", ";
            }
            privilegios += listPrivilegios[listPrivilegios.length - 1];
        }
        String component = Constants.EMPLOYEE_UPDATE_FAIL;
        if (contrasena.equals(confirmacion)) {
            log.debug("Campos formulario de registro OK");
            Empleado empleado = new Empleado(dni, contrasena, privilegios);
            try {
                Dao<Empleado, Integer> empleadoDao = Util.getDao(Empleado.class);
                int statusCode = empleadoDao.create(empleado);
                if (statusCode == 1) { // Si se ha insertado correctamente
                    component = Constants.EMPLOYEE_UPDATE_SUCCESS;
                    log.debug("Actualizado empleado: " + empleado.toString());
                }
            } catch (SQLException e) {
                log.error("Error al acceder a la base de datos", e);
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(component);
        dispatcher.forward(request, response);
    }

    /**
     * Inicia sesión como empleado
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    static void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String codEmpleado = request.getParameter("cod_empleado");
        String password = request.getParameter("password");
        log.debug("Intentando loguearse con parametros  " + codEmpleado + ":" + password);
        String component = Constants.LOGIN_FAIL;
        if (!codEmpleado.equals("") && !password.equals("")) { // Si los campos son correctos
            log.debug("Campos OK");
            try {
                Dao<Empleado, Integer> empleadoDao = Util.getDao(Empleado.class);
                Empleado empleado = empleadoDao.queryForId(Integer.parseInt(codEmpleado));
                if (empleado != null) {  // Si el empleado está registrado
                    if (empleado.getContrasena().equals(Util.sha1(password))) { // La contraseña coincide
                        HttpSession session = request.getSession();
                        session.setAttribute(Constants.EMPLOYEE_BEAN, empleado);
                        log.debug("Loggeado el empleado: " + empleado.toString());
                        component = Constants.EMPLOYEE_LOGIN_SUCCESS;
                    }
                } else {
                    log.error("No hay nadie registrado con ese codigo: " + codEmpleado);
                }
            } catch (SQLException e) {
                log.error("Error en la query a la base de datos: ", e);
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(component);
        dispatcher.forward(request, response);
    }

    /**
     * Registra un nuevo empleado
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    public static void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String dni = request.getParameter("dni");
        String[] listPrivilegios = request.getParameterValues("privilegios");
        String contrasena = request.getParameter("contrasena");
        String confirmacion = request.getParameter("re_contrasena");

        // concatena los privilegios en una lista separada por comas
        String privilegios = "";
        if (listPrivilegios.length > 0) {
            for (int i = 0; i < listPrivilegios.length - 1; i++) {
                privilegios += listPrivilegios[i] + ", ";
            }
            privilegios += listPrivilegios[listPrivilegios.length - 1];
        }
        Empleado empleado1 = new Empleado(dni, contrasena, privilegios);
        log.debug("Registrado empleado: " + empleado1.toString());

        String component = Constants.EMPLOYEE_REGISTER_FAIL;
        if (contrasena.equals(confirmacion)) {
            log.debug("Campos formulario de registro OK");
            Empleado empleado = new Empleado(dni, contrasena, privilegios);
            try {
                Dao<Empleado, Integer> empleadoDao = Util.getDao(Empleado.class);
                int statusCode = empleadoDao.create(empleado);
                if (statusCode == 1) { // Si se ha insertado correctamente
                    component = Constants.EMPLOYEE_REGISTER_SUCCESS;
                    log.debug("Registrado empleado: " + empleado.toString());
                }
            } catch (SQLException e) {
                log.error("Error al acceder a la base de datos", e);
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(component);
        dispatcher.forward(request, response);
    }

    /**
     * Elimina un empleado
     *
     * @param request  Petición http
     * @param response Respuesta http
     */
    public static void remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Empleado empleado = (Empleado) session.getAttribute(Constants.EMPLOYEE_BEAN);
        session.removeAttribute(Constants.EMPLOYEE_BEAN);
        try {
            Dao<Empleado, Integer> empleadoDao = Util.getDao(Empleado.class);
            empleadoDao.delete(empleado);
            log.debug("Eliminado " + empleado.toString());
        } catch (SQLException e) {
            log.error("Error al eliminar un empleado");
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(Constants.EMPLOYEE_PROFILE_REMOVED);
        dispatcher.forward(request, response);
    }
}
