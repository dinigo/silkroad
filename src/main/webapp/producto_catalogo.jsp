<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<sql:query var="list_sellers" dataSource="jdbc/tienda">
    SELECT * FROM PRODUCTOCATALOGO WHERE "codVendedor"=${param.codVendedor} AND "codProducto"=${param.codProducto}
</sql:query>
<c:set var="producto" scope="page" value="${list_sellers.rows[0]}"/>

<html>
<head>
    <title>EDITAR PRODUCTO</title>
</head>
<body>

<h1>PRODUCTO DEL CATÁLOGO</h1>

<h2>EDITAR </h2>
<form method="post" action="/controler">
    Precio: <input type="text" name="precio" value="${producto.precio}"><br>
    Stock: <input type="text" name="stock" value="${producto.stock}"><br>
    Activo:
    Si<input type="radio" name="activo" value="true" <s:if test="${producto.activo == true}">checked</s:if>>
    No<input type="radio" name="activo" value="false" <s:if test="${producto.activo == false}">checked</s:if>><br>
    <input type="hidden" name="codProducto" value="${param.codProducto}">
    <input type="hidden" name="codVendedor" value="${param.codVendedor}">
    <input type="hidden" name="form_type" value="update_productocatalogo">
    <button type="submit">Actualizar</button>
</form>

<h2>ELIMINAR</h2>

<form method="post" action="/controler">
    <input type="hidden" name="codProducto" value="${param.codProducto}">
    <input type="hidden" name="codVendedor" value="${param.codVendedor}">
    <input type="hidden" name="form_type" value="remove_productocatalogo">
    <button type="submit">Eliminar</button>
</form>

</body>
</html>