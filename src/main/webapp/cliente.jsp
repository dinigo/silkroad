<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="clientBean" scope="session" type="com.silkroad.data.Cliente"/>
<html>
<head>
    <title>PERFIL</title>
</head>
<body>
<h1>PERFIL</h1>

<h2>CAMBIAR DATOS PERSONALES</h2>
<form method="post" action="/controler">
    Nombre: <input type="text" name="nombre" value="${clientBean.nombre}">
    Apellidos <input type="text" name="apellidos" value="${clientBean.apellidos}"><br>
    Dirección: <input type="text" name="direccion" value="${clientBean.direccion}"><br>
    DNI: <input type="text" name="dni" value="${clientBean.dni}"><br>
    email: <input type="text" name="email" value="${clientBean.email}"><br>
    <input type="hidden" name="form_type" value="update_cliente">
    <button type="submit">Actualizar</button>
</form>

<h2>CAMBIAR CONTRASEÑA</h2>
<form method="post" action="/controler">
    Contraseña Actual: <input type="password" name="vieja_contrasena"><br>
    Nueva Contraseña: <input type="password" name="nueva_contrasena">
    Confirmar contraseña: <input type="password" name="re_contrasena"><br>
    <input type="hidden" name="form_type" value="change_password">
    <button type="submit">Cambiar</button>
</form>

<h2>BORRAR CLIENTE</h2>
<form method="post" action="/controler">
    <input type="hidden" name="form_type" value="remove_cliente">
    <button type="submit">Borrar</button>
</form>
</body>
</html>