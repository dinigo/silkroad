<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="empleadoBean" scope="session" type="com.silkroad.data.Empleado"/>
<html>
<head>
    <title>PERFIL DE EMPLEADO</title>
</head>
<body>
<h1>PERFIL</h1>

<form action="/controler" method="post">
    Código: ${empleadoBean.codEmpleado}
    DNI: ${empleadoBean.codEmpleado}
    Privilegios para modificar:
    <ul>
        <li><input type="checkbox" name="privilegios" value="empleados" checked=<%= empleadoBean.canModify("empleados")%>>Empleados</li>
        <li><input type="checkbox" name="privilegios" value="clientes" checked=<%= empleadoBean.canModify("clientes")%>>Clientes</li>
        <li><input type="checkbox" name="privilegios" value="vendedores" checked=<%= empleadoBean.canModify("vendedores")%>>Vendedores</li>
        <li><input type="checkbox" name="privilegios" value="catalogo" checked=<%= empleadoBean.canModify("catalogo")%>>Catálogo</li>
        <li><input type="checkbox" name="privilegios" value="pedidos" checked=<%= empleadoBean.canModify("pedidos")%>>Pedidos</li>
    </ul>
    Contraseña: <input type="password" name="contrasena">
    Confirmar contraseña: <input type="password" name="re_contrasena"><br>
    <input type="hidden" name="form_type" value="update_empleado">
    <button type="submit">Actualizar</button>
</form>

<form action="/controler" method="post">
    <input type="hidden" name="form_type" value="remove_empleado">
    <button type="submit">Eliminar</button>
</form>
</body>
</html>