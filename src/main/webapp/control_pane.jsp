<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>PANEL DE CONTROL</title>
</head>
<body>
<h1>PANEL DE CONTROL</h1>

<div style="float:left;">
    <h2>GESTION DE VENDEDORES</h2>

    <h3>NUEVO VENDEDOR</h3>

    <form method="post" action="/controler">
        Nombre: <input type="text" name="nombre"><br>
        CIF <input type="text" name="cif"><br>
        Teléfono: <input type="text" name="telefono"><br>
        email: <input type="text" name="email"><br>
        Dirección: <input type="text" name="direccion"><br>
        Activo:
        Si<input type="radio" name="activo" value="true" checked>
        No<input type="radio" name="activo" value="false"><br>
        <input type="hidden" name="form_type" value="register_vendedor">
        <button type="submit">Registrar</button>
    </form>

    <h3>LISTA DE VENDEDORES</h3>
    <sql:query var="vendedor_list" dataSource="jdbc/tienda">
        SELECT * FROM VENDEDOR
    </sql:query>
    <table>
        <tr>
            <td>#</td>
            <td>Nombre</td>
        </tr>
        <c:forEach var="vendedor" items="${vendedor_list.rows}">
            <tr>
                <td><a href="vendedor.jsp?codVendedor=${vendedor.codVendedor}">${vendedor.codVendedor}</a></td>
                <td><a href="vendedor.jsp?codVendedor=${vendedor.codVendedor}">${vendedor.nombre}</a></td>
            </tr>
        </c:forEach>
    </table>
</div>
<div style="float:left;">
    <h2>GESTIÓN DE PRODUCTOS</h2>

    <h3>REGISTRAR PRODUCTO</h3>

    <form method="post" action="/controler">
        Categoría: <input type="text" name="categoria" value="${currentProduct.categoria}"><br>
        Marca: <input type="text" name="marca" value="${currentProduct.marca}"><br>
        Modelo: <input type="text" name="modelo" value="${currentProduct.modelo}"><br>
        Talla:
        <select name="talla">
            <option value="s">Pequeña</option>
            <option value="m">Mediana</option>
            <option value="l">Grande</option>
        </select><br>
        Descripción: <input type="text" name="descripcion" value="${currentProduct.descripcion}"><br>
        Activo:
        Si<input type="radio" name="activo" value="true" checked>
        No<input type="radio" name="activo" value="false"><br>
        <input type="hidden" name="form_type" value="register_producto">
        <button type="submit">Registrar</button>
    </form>

    <h3>LISTA DE PRODUCTOS</h3>
    <sql:query var="product_list" dataSource="jdbc/tienda">
        SELECT * FROM PRODUCTO
    </sql:query>
    <table>
        <tr>
            <td> # </td>
            <td> Categoría </td>
            <td> Marca </td>
            <td> Modelo </td>
            <td> Talla </td>
        </tr>
        <c:forEach items="${product_list.rows}" var="producto">
            <tr>
                <td><a href="producto.jsp?codProducto=${producto.codProducto}"> ${producto.codProducto} </a></td>
                <td> ${producto.categoria} </td>
                <td> ${producto.marca} </td>
                <td> ${producto.modelo} </td>
                <td> ${producto.talla} </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>