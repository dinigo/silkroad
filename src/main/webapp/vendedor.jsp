<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<sql:query var="list_sellers" dataSource="jdbc/tienda">
    SELECT * FROM VENDEDOR WHERE "codVendedor"=${param.codVendedor}
</sql:query>
<c:set var="producto" scope="page" value="${list_sellers.rows[0]}"/>

<html>
<head>
    <title>VENDEDOR</title>
</head>
<body>
<h1>VENDEDOR</h1>

<div style="float:left;">

    <h2>PERFIL</h2>

    <form method="post" action="/controler">
        Nombre: ${producto.nombre}<br>
        CIF: ${producto.cif}<br>
        Teléfono: <input type="text" name="direccion" value="${producto.telefono}"><br>
        email: <input type="text" name="email" value="${producto.email}"><br>
        Dirección: <input type="text" name="dni" value="${producto.direccion}"><br>
        Activo:
        Si<input type="radio" name="activo" value="true" <s:if test="${producto.activo == true}">checked</s:if>>
        No<input type="radio" name="activo" value="false"
                 <s:if test="${producto.activo == false}">checked</s:if>><br>
        <input type="hidden" name="form_type" value="update_vendedor">
        <button type="submit">Actualizar</button>
    </form>

    <h2>BORRAR VENDEDOR</h2>

    <form method="post" action="/controler">
        <input type="hidden" name="form_type" value="remove_vendedor">
        <button type="submit">Borrar</button>
    </form>
</div>

<div style="float:left;">
    <h2>AÑADIR AL CATÁLOGO</h2>
    <sql:query var="product_list" dataSource="jdbc/tienda">
        SELECT * FROM PRODUCTO WHERE "activo"=true
    </sql:query>
    <form method="post" action="/controler">
        Producto:
        <select name="codProducto">
            <c:forEach items="${product_list.rows}" var="producto">
                <option value="${producto.codProducto}">${producto.marca} - ${producto.modelo} - ${producto.talla} - ${producto.descripcion}</option>
            </c:forEach>
        </select><br>
        Precio: <input type="number" name="precio">
        Stock: <input type="number" name="stock">
        Activo:
        Si<input type="radio" name="activo" value="true" checked>
        No<input type="radio" name="activo" value="false"><br>
        <input type="hidden" name="codVendedor" value="${param.codVendedor}">
        <input type="hidden" name="form_type" value="register_productocatalogo">
        <button type="submit">Añadir</button>
    </form>
    <h3>CATÁLOGO</h3>
    <sql:query var="product_list" dataSource="jdbc/tienda">
        SELECT * FROM PRODUCTOCATALOGO WHERE "codVendedor"=${param.codVendedor}
    </sql:query>
    <table>
        <tr>
            <td>#</td>
            <td>codProducto</td>
            <td>Precio</td>
            <td>Stock</td>
            <td>Activo</td>
        </tr>
        <c:forEach items="${product_list.rows}" var="producto">
            <tr>
                <td>
                    <a href="producto_catalogo.jsp?codProducto=${producto.codProducto}&codVendedor=${param.codVendedor}">${producto.codProducto}</a>
                </td>
                <td>${producto.codProducto}</td>
                <td>${producto.precio}</td>
                <td>${producto.stock}</td>
                <td>${producto.activo}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>