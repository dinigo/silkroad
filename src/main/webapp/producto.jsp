<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<sql:query var="producto" dataSource="jdbc/tienda">
    SELECT * FROM PRODUCTO WHERE "codProducto"=${param.codProducto}
</sql:query>

<c:set var="currentProduct" scope="page" value="${producto.rows[0]}"/>

<html>
<head>
    <title>EDITAR PRODUCTO</title>
</head>
<body>

<h1>EDITAR PRODUCTO</h1>

<form method="post" action="/controler">
    Categoría: ${currentProduct.categoria}<br>
    Marca: ${currentProduct.marca}<br>
    Modelo: ${currentProduct.modelo}<br>
    Talla: ${currentProduct.talla}<br>

    <form method="post" action="/controler">
        Descripción:<br><textarea name="descripcion" rows="4">${currentProduct.descripcion}"</textarea><br>
        Activo:
        Si<input type="radio" name="activo" value="true" <s:if test="${currentProduct.activo == true}">checked</s:if>>
        No<input type="radio" name="activo" value="false" <s:if test="${currentProduct.activo == false}">checked</s:if>><br>
        <input type="hidden" name="codProducto" value="${param.codProducto}">
        <input type="hidden" name="form_type" value="update_producto">
        <button type="submit">Actualizar</button>
    </form>

    <h1>BORRAR PRODUCTO</h1>

    <form method="post" action="/controler">
        <input type="hidden" name="codProducto" value="${param.codProducto}">
        <input type="hidden" name="form_type" value="remove_producto">
        <button type="submit">Eliminar</button>
    </form>
</body>
</html>