CREATE TABLE IF NOT EXISTS "PRODUCTO" (
    "codProducto" INTEGER NOT NULL IDENTITY,
    "categoria"   VARCHAR(50) NOT NULL,
    "marca"       VARCHAR(50) NOT NULL,
    "modelo"      VARCHAR(50) NOT NULL,
    "talla"       VARCHAR(50) NOT NULL,
    "descripcion" VARCHAR(1000) NOT NULL,
    "activo"      BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS "VENDEDOR" (
    "codVendedor" INTEGER NOT NULL IDENTITY,
    "nombre"      VARCHAR(50) NOT NULL,
    "cif"         VARCHAR(50) NOT NULL,
    "telefono"    VARCHAR(50) NOT NULL,
    "email"       VARCHAR(50) NOT NULL,
    "direccion"   VARCHAR(500) NOT NULL,
    "activo"      BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS "EMPLEADO" (
    "codEmpleado" INTEGER NOT NULL IDENTITY,
    "dni"         VARCHAR(50) NOT NULL,
    "contrasena"  VARCHAR(50) NOT NULL,
    "privilegios" VARCHAR(50) NOT NULL,
);

CREATE TABLE IF NOT EXISTS "CLIENTE" (
    "codCliente"  INTEGER NOT NULL IDENTITY,
    "nombre"      VARCHAR(50) NOT NULL,
    "apellidos"   VARCHAR(50) NOT NULL,
    "direccion"   VARCHAR(500) NOT NULL,
    "dni"         VARCHAR(50) NOT NULL,
    "email"       VARCHAR(50) NOT NULL,
    "contrasena"  VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS "PRODUCTOCATALOGO" (
    "codCatalogo" INTEGER NOT NULL IDENTITY,
    "codProducto" INTEGER NOT NULL,
    "codVendedor" INTEGER NOT NULL,
    "precio"      FLOAT NOT NULL,
    "stock"       INTEGER NOT NULL,
    "activo"      BOOLEAN NOT NULL,
--     PRIMARY KEY ("codProducto", "codVendedor"),
    FOREIGN KEY ("codProducto") REFERENCES "PRODUCTO" ("codProducto") ON DELETE CASCADE,
    FOREIGN KEY ("codVendedor") REFERENCES "VENDEDOR" ("codVendedor") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "PEDIDO" (
    "codPedido"   INTEGER NOT NULL IDENTITY,
    "codCliente"  INTEGER NOT NULL,
    "precio"      INTEGER NOT NULL,
    "stock"       INTEGER NOT NULL,
    "activo"      INTEGER NOT NULL,
    FOREIGN KEY ("codCliente") REFERENCES "CLIENTE" ("codCliente") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "LINEAPEDIDO" (
    "codPedido"       INTEGER NOT NULL,
    "codProducto"     INTEGER NOT NULL,
    "codVendedor"     INTEGER NOT NULL,
    "cantidad"        INTEGER NOT NULL,
    "estadoProducto"  VARCHAR(50) NOT NULL,
    "fechaEnvio"      DATE,
    "fechaEntrega"    DATE,
    PRIMARY KEY ("codPedido", "codProducto", "codVendedor"),
    FOREIGN KEY ("codPedido") REFERENCES "PEDIDO" ("codPedido") ON DELETE CASCADE,
    FOREIGN KEY ("codProducto") REFERENCES "PRODUCTO" ("codProducto") ON DELETE CASCADE,
    FOREIGN KEY ("codVendedor") REFERENCES "VENDEDOR" ("codVendedor") ON DELETE CASCADE
);

-- Crea las secuencias para usar el autoincremento desde el dao
-- Ojo! solo se puede ejecutar una vez porque hsqldb no soporta una "IF NOT EXISTS" en secuencias
CREATE SEQUENCE "PRODUCTO_ID_SEQ" START WITH 1;
CREATE SEQUENCE "VENDEDOR_ID_SEQ" START WITH 1;
CREATE SEQUENCE "CLIENTE_ID_SEQ" START WITH 1;
CREATE SEQUENCE "PEDIDO_ID_SEQ" START WITH 1;
CREATE SEQUENCE "EMPLEADO_ID_SEQ" START WITH 1;
CREATE SEQUENCE "PRODUCTOCATALOGO_ID_SEQ" START WITH 1;