#!/bin/bash

# Si hay una sesión de hsqldb ejecutandose la mata
if  screen -list | grep -q "hsqldb"; then
    screen -X -S hsqldb quit
fi
# Si hay una carpeta de tomcat intenta cerrarlo
if [ -d "tomcat" ]; then
    tomcat/bin/shutdown.sh > /dev/null
fi
rm -rf tomcat hsqldb config
mvn clean -q
