DISCLAIMER 
==========
Este proyecto es la parte práctica de la asignatura de **Aplicaciones Telemáticas** de la universidad. No pretende en 
ningún caso tener un uso comercial o siquiera llegarse a usar como herramienta. Sin embarto se emplean tecnologías del
estado del arte de la web contemporanea por lo que puede ser base a un futuro desarrollo más serio.

HERRAMIENTA
===========
Se trata de la web para una tienda. Esta está organizada empleando páginas en html, jsp y servlets para las partes que
 requieran programas mas complejos. Siempre documentado y empleando en la medida de lo posible buenas prácticas.

TECNOLOGÍAS
===========
- **Maven2**: para compilar, testear, empaquetar y desplegar el código, y para gestionar las dependencias.
- **Tomcat7**: como servidor de aplicaciones java/web/jsp ligero y de facil instalación y configuración en entornos
UNIX.
- **Hsqldb**: de base de datos

EJECUCIÓN
=========
Para la compilación y despliegue de la aplicación se requiere:
- **Maven2**: es la herramienta encargada de gestionar este proceso.
- **Tomcat7+**: maven descarga y ejecuta uno en local si no se le indica uno.
- **Hsqldb**: para la persistencia de datos.
- **Conexión a Internet**: por si alguna de las dependencias no se hubiese descargado.

CONFIGURACIÓN
=============

### Tomcat7+
Añadir un usuario que pueda utilizar maven para hacer el despliegue. Este usuario debe tener permisos de 
`manager-script`. Se debe editar el archivo de configuración `${CATALINA_HOME}/conf/silkroad.log` o `/etc/tomcat7/tomcat-users.xml` de la siguiente forma:
```xml
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
    <!-- rol con acceso a /manager/text para hacer deploy -->
    <role rolename="manager-script"/>
    <!-- usuario para hacer deploy desde maven -->
    <user username="maven" password="s3cr3t" roles="manager-script"/>
</tomcat-users>
```

Para el acceso a la base ede datos se configura un `resource` nuevo en el context. Puede hacerse tanto en el servidor
`${CATALINA_HOME}/conf/context.xml` como en el propio proyecto, en `${PROJECT_HOME}/src/main/webapp/META-INF/context.xml`
(siguiendo la gerarquía de carpetas de maven), dependiendo de si se quiere dar visibilidad a todos los 
artefactos o proyectos desplegados o solo al nuestro.
```xml
<?xml version='1.0' encoding='utf-8'?>
<Context>
    <Resource name="jdbc/tienda" auth="Container" type="javax.sql.DataSource"
        maxActive="100" maxIdle="30" maxWait="10000"
        username="usuarioDb" password="claveDb" driverClassName="org.hsqldb.jdbc.JDBCDriver"
        url="jdbc:hsqldb:hsql://localhost:11000/tienda"/>
</Context>
```

Finalmente, se puede cambiar el puerto por el que se sirven las aplicaciones en `${CATALINA_HOME}/conf/context.xml` en
la linea:
```
...
<Connector port="8888" protocol="HTTP/1.1"
    URIEncoding="UTF-8"
    connectionTimeout="20000"
    redirectPort="8443" />
...
```
### Maven
Ahora tenemos dos opciones: o escirbir el usuario y contraseña en claro en el pom.xml (mala idea si se usa control de 
versiones porque podría hacerse público), o añadir un servidor al archivo de configuración de maven 
`${HOME}/.m2/settings.xml` y después hacer referencia a él:
```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
        <server>
            <id>MiServidor</id>
            <username>maven</username>
            <password>s3cr3t</password>
        </server>
    </servers>
</settings>
```

### Hsqldb
Configurar la base de datos, esto es, el archivo en que se guardará y los credenciales, así como el pueto por el que se 
va a servir. Abría que asegurarse que el puerto está por encima de 1024 o ejecutar el servidor con privilegios de root 
en caso contrario.

El archivo de configuración `{HSQLDB_HOME}/server.properties` es de la siguiente forma:
```
server.port=11000
server.database.0=file:databases/tienda;user=usuarioDb;password=claveDb
server.dbname.0=tienda
```


CONEXIÓN
========
Este apartado trata de dar una idea de los puertos, direcciones y máquinas en las que se ejecuta cada servicio:

| Servicio  |             Dirección              |
|:--------: |:----------------------------------:|
|  HSQLDB   |  jdbc:hsqldb:hsql://ip:port/tienda |
|  Tomcat8  |  http://ip:port/silkroad           |


LOGS
====
El código incluye logs de diferentes niveles. Estos se realizan a traves de la fachada de logs de SLF4 con la librería 
`logback`. Se almacenan en el archivo `${CATALINA_HOME}/bin/silkroad.log`. El formato de los logs y los __appenders__ se
pueden configurar en `${PROJECT_HOME}/src/main/resources/logback.xml`. Además `logback` proporciona un servlet para
visualizar los logs en `html`, basta con mapearlo en `web.xml`.


ACCESO A LA BASE DE DATOS
=========================
Tras escribir las peticiones la base de datos a mano (hard-coded) realicé una clase que trataba de abstraer la conexión
mediante diferentes funciones que hacían diferentes tipos de petición. Estas funciones devolvían objetos que extendían 
cierta interfaz `Consumible`, que les obligaba a instanciarse recuperando los datos de la respuesta a la petición o a 
generar una.

Después encontré el patrón de diseño DAO (Data Access Object) que abstrae la instanciación de las peticiones, haciendo
uso de capa de `Factorys` que se encargan de serializar y deserializar los `POJO`. Este estaba implementado en varias
librerías haciendo redundante (y sensible a fallos) implementarlo otra vez así que comencé a usar ORMlite.

Sin embargo al haber abstraido la conexión a la base de datos (la realiza tomcat) y ser las versiones de hsqldb 
absolutamente incompatibles dada la nueva sintaxis la clase que debería construir las querys no tenía conocimiento de
la versión de hsqldb ni del driver por lo que utilizaba la versión antigua. Esto se soluciona sobrecargando la función
que lo comprobaba a modo de clase interna anónima.

Esta librería permite utilizar las anotaciones de java para persistencia de datos (@Entity, @Column, @Id, etc) o las
suyas propias (@DatabaseTable, @DatabaseColumn, etc).
