#!/bin/bash

DEPENDENCES="unzip maven xdg-utils git openjdk-7-jdk"
HSQLDB_VERSION="2.3.2"
TOMCAT_VERSION="8.0.8"
HSQLDB_FILE="hsqldb-${HSQLDB_VERSION}.zip"
TOMCAT_FILE="apache-tomcat-${TOMCAT_VERSION}.zip"
CONFIG_REPO="https://gist.github.com/f23ea1adc5aed941b016.git"
HSQLDB_DOWNLOAD="http://skylink.dl.sourceforge.net/project/hsqldb/hsqldb/hsqldb_2_3/${HSQLDB_FILE}"
TOMCAT_DOWNLOAD="http://ftp.cixug.es/apache/tomcat/tomcat-8/v8.0.8/bin/${TOMCAT_FILE}"

# Instala dependencias
echo "***Descargando:"
echo "      dependencias"
sudo -S apt-get -qq -y install $DEPENDENCES

# Descarga
echo "      configuración"
git clone --quiet $CONFIG_REPO config
echo "      hsqldb"
wget --quiet $HSQLDB_DOWNLOAD
echo "      tomcat"
wget --quiet $TOMCAT_DOWNLOAD

# Descomprime
echo "***Descomprimiendo:"
echo "      hsqldb"
unzip -q $HSQLDB_FILE
mv hsqldb-$HSQLDB_VERSION/hsqldb/ .
echo "      tomcat"
unzip -q $TOMCAT_FILE
mv apache-tomcat-$TOMCAT_VERSION tomcat

# Configura
echo "***Configurando:"
echo "      hsqldb"
cp config/server.properties hsqldb/
echo "      tomcat"
chmod +x tomcat/bin/*.sh
rm tomcat/conf/server.xml tomcat/conf/tomcat-users.xml tomcat/conf/context.xml
cp config/server.xml config/tomcat-users.xml config/context.xml tomcat/conf/
cp hsqldb/lib/hsqldb.jar tomcat/lib/
echo "      maven"
mkdir -p ~/.m2
cp config/settings.xml ~/.m2/
echo "      iptables (tcp 8888)"
sudo iptables -A INPUT -p tcp -m tcp --dport 8888 -j ACCEPT


# Limpia
rmdir hsqldb-$HSQLDB_VERSION
rm $HSQLDB_FILE
rm $TOMCAT_FILE
rm -rf config

# Ejecuta 
echo "***Ejecutando:"
echo "      hsqldb"
cd hsqldb
screen -dmS hsqldb java -cp lib/hsqldb.jar org.hsqldb.Server
cd ..
echo "      tomcat"
cd tomcat
bin/startup.sh > /dev/null
cd ..

# Compila y hace deploy de la aplicación
echo "***Compilando y desplegando"
mvn tomcat7:deploy -q > /dev/null


# Muestra un enlace a la app
echo "***Listo:"
EXT_IP=`wget -qO- ipecho.net/plain`
LOCAL_IP=`ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'`
IP="localhost"
# si la ip interna y externa coinciden estoy en un servidor, sino estoy en local
if [ ${EXT_IP} -eq ${LOCAL_IP} ]; then
    IP=EXT_IP
fi
echo "      http://${IP}:8888/silkroad"

# Abre el navegador
#xdg-open http://localhost:8888/silkroad